<?php
/**
 * Created by YUNYI.
 * User: james.xiao
 * Date: 2021-03-19
 * Time: 13:54
 */

namespace yy\helper;


class Str
{
    /**
     * 正则查找返回匹配的第一条
     * @param $pattern 正则
     * @param $subject 目标
     * @return string
     */
    static function strPregMatch($pattern, $subject){
        if(preg_match($pattern, $subject , $res))return $res[0];
        return '';
    }

    /**
     * 生成唯一的数字
     * @param int $num 长度
     * @return bool|string
     */
    public static function generate($num = 8)
    {
        $data = substr(base_convert(md5(uniqid(md5(microtime(true)),true)), 16, 10), 0, $num);

        return $data;
    }

    /**
     * 文件扩展名
     * @param $file
     * @return mixed
     */
    public static function getFileExt($file)
    {
        return pathinfo($file, PATHINFO_EXTENSION);

    }

    /**
     * 移除表情符号
     * @param $text
     * @return string
     */
    public static function removeEmoji($text)
    {
        if ($text === '') {
            return '';
        }

        $len     = mb_strlen($text);
        $newText = '';
        for ($i = 0; $i < $len; $i++) {
            $str = mb_substr($text, $i, 1, 'utf-8');
            if (strlen($str) >= 4)
                continue;//emoji表情为4个字节
            $newText .= $str;
        }

        return $newText;
    }


    /**
     * 随机颜色值（16位）
     * @return string
     */
    public static function getColorRandom()
    {
        $str = '0123456789ABCDE';

        $color = '#';
        for ($i = 0; $i < 6; $i++) {
            $color .= $str[rand(0, strlen($str) - 1)];
        }

        return $color;
    }


    /**
     * 生成永远唯一的密钥码
     * sha512(返回128位) sha384(返回96位) sha256(返回64位) md5(返回32位)
     * 还有很多Hash函数......
     * @param int    $type 返回格式：0大小写混合  1全大写  2全小写
     * @param string $func 启用算法：
     * @return string
     * @author xiaochaun
     */
    public static function getUnique($type = 0, $func = 'md5')
    {
        $uid  = md5(uniqid(rand(), true) . microtime());
        $hash = hash($func, $uid);
        $arr  = str_split($hash);
        foreach ($arr as $v) {
            if ($type == 0) {
                $newArr[] = empty(rand(0, 1)) ? strtoupper($v) : $v;
            }
            if ($type == 1) {
                $newArr[] = strtoupper($v);
            }
            if ($type == 2) {
                $newArr[] = $v;
            }
        }

        return implode('', $newArr);
    }

}