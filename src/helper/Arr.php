<?php
/**
 * Created by YUNYI.
 * User: james.xiao
 * Date: 2021-03-27
 * Time: 15:21
 */

namespace yy\helper;


class Arr
{
    /**
     * 判断数组是否相等 只比对值
     * @param $array1
     * @param $array2
     * @return bool true 相等
     */

    public static function eq($array1 , $array2)
    {
        sort($array1);
        sort($array2);
        return $array1 == $array2;

    }

    /**
     * 判断一个数组是否包含另一个数组 只比对值
     * @param $array1
     * @param $array2
     * @return bool true 包含
     */
    public static function contain($array1, $array2)
    {
        return ! (bool) array_diff($array2 , $array1);
    }

    /**
     * 一维数组排序
     * 按指定排序数组返回新数组
     * eg:
     * $source = ['历史','化学','地理','政治','数学','文数','文综','物理','理数','理综','生物','英语','语文'];
     * $sort = ['语文','数学','英语','物理','化学','生物','历史','地理','政治']
     * return ["语文","数学","英语","物理","化学","生物","历史","地理","政治","文数","文综","理数","理综"]
     * @param $source array 要处理数组
     * @param $sort array 指定排序数组
     * @return array
     */
    public static function sortByArray(array $source ,array $sort)
    {
        // 先取交集
        $intersect = array_intersect($sort , $source);
        // 取在目标数组不在排序数组
        $diff = array_diff($source , $sort);
        // 合并数组
        return array_merge($intersect , $diff);

    }

    /**
     * 二维数组排序
     * 按指定排序数组返回新数组
     * eg:
     * $source = [
        [
        'id' => 1,
        'name' => '数学'
        ],
        [
        'id' => 2,
        'name' => '语文'
        ],
        [
        'id' => 3,
        'name' => '化学'
        ],
    ];
    $sort = ['语文','数学'];
    return [
        [
        'id' => 2,
        'name' => '语文'
        ],
        [
        'id' => 1,
        'name' => '数学'
        ],
        [
        'id' => 3,
        'name' => '化学'
        ],
    ];
     * @param array $source 要处理数组
     * @param array $sort 指定排序数组
     * @param string $field 处理字段
     * @return array
     */
    public static function multiSortByArray(array $source ,array $sort , string $field = 'name')
    {
        $new_sort = array_flip($sort);
        $data = $other =  [];
        foreach ($source as $row) {
            $index = $new_sort[$row[$field]] ?? false;
            if ($index !== false){
                $data[$index] = $row;
            } else {
                $other[] = $row;
            }

        }
        ksort($data);
        return array_merge($data , $other);
    }

}