<?php
// +----------------------------------------------------------------------
// | Created by YUNYI.
// +----------------------------------------------------------------------
// | Licensed ( http://www.yunyiit.com )
// +----------------------------------------------------------------------
// | Author: james.xiao.
// +----------------------------------------------------------------------
declare (strict_types = 1);

//------------------------
// 自动生成类库
//-------------------------

namespace app\model;


/**
 * ${demo_name}
 * Class demo
 * @package app\model
 */
class demo extends Common
{
    public function getList($param, $where = false, $order = false, $fun = false)
    {
        $where = function ($query) use ($param , $where){
            $query->where($where);
            // 搜索
            if (!empty($param['search'])){

            }
            // 日期筛选
            if (!empty($param['create']) && is_array($param['create'])){
                $query->where('create_time' , '>=' , strtotime($param['create'][0]));
                $query->where('create_time' , '<=' , strtotime($param['create'][1] . '23:59'));
            }
        };
        return parent::getList($param, $where, $order, $fun);
    }

}