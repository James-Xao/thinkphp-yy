<?php
// +----------------------------------------------------------------------
// | Created by YUNYI.
// +----------------------------------------------------------------------
// | Licensed ( http://www.yunyiit.com )
// +----------------------------------------------------------------------
// | Author: james.xiao.
// +----------------------------------------------------------------------
declare (strict_types = 1);

//------------------------
// 自动生成类库
//-------------------------

namespace app\facade;


use think\Facade;

/**
 * ${demo_name}
 * @see \app\model\demo
 * @mixin  \app\model\demo
 */
class demo extends Facade
{

    protected static function getFacadeClass()
    {
        return 'app\model\demo';
    }

}