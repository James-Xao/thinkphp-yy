<?php
// +----------------------------------------------------------------------
// | Created by YUNYI.
// +----------------------------------------------------------------------
// | Licensed ( http://www.yunyiit.com )
// +----------------------------------------------------------------------
// | Author: james.xiao.
// +----------------------------------------------------------------------
declare (strict_types = 1);

//------------------------
// 自动生成类库
//-------------------------

namespace app\validate;


use think\Validate;

/**
 * ${demo_name}
 * Class demo
 * @package app\validate
 */
class demo extends Validate
{
    public function sceneEdit()
    {
        $this->rule = [
            // 'name|名称' => 'require',
            ];
    }

}