<?php

namespace yy\build;

use think\helper\Str;

class Build
{
    /**
     * 创建类
     * @param $className 类名
     * @param $name 描述
     */
    public static function run($className ,$name)
    {
        $className = Str::studly($className);
        self::buildFile($className ,$name , 'controller');
        self::buildFile($className ,$name , 'model');
        self::buildFile($className ,$name , 'facade');
        self::buildFile($className ,$name , 'validate');
    }


    /**
     * @param $className 类名
     * @param $type 类型
     */
    protected static function buildFile($className , $name , $type)
    {
        $appPath = app_path();
        $classPath = $appPath . $type;

        self::checkDirBuild($classPath);
        $saveName = $classPath . '/' . $className . '.php';
        if (file_exists($saveName))return;
        $path = __DIR__ . '/template/';
        $file = file_get_contents($path . $type . '.php');
        $file = str_replace('${demo_name}' , $name , $file);
        $file = str_replace('demo' , $className , $file);
        self::writeFile($saveName  , $file);
    }

    /**
     * 写入文件
     * @param $fileName
     * @param $data
     */
    public static function writeFile($fileName , $data)
    {
        file_put_contents($fileName , $data);

    }

    /**
     * 创建目录
     * @access protected
     * @param  string $dirname 目录名称
     * @return void
     */
    protected static function checkDirBuild($dirname)
    {
        !is_dir($dirname) && mkdir($dirname, 0755, true);
    }
}
