<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/5/5
 * Time: 11:48
 */

namespace yy\build;


use think\facade\Db;
use think\helper\Str;

/**
 * 生成api文档
 * Class SeedApi
 * @package yy\build
 */
class SeedApi
{
    protected $projectId;

    public function __construct($projectId)
    {
        $this->projectId = $projectId;
    }
    public static function init($projectId)
    {
        return new self($projectId);

    }

    public function seed($name , $table)
    {
        $arr = [
            'projectId' => $this->projectId,
            'type' => 0,
            'parentId' => 0,
            'name' => $name
        ];
        $pid = $this->db()->insertGetId($arr);
        $op = [
            'getList' => [
                'name' => '列表',
                'method' => 'GET',
                'send' => '[{"name":"curr","type":"int","desc":"","subFieldList":[]},{"name":"row","type":"int","desc":"","subFieldList":[]},{"name":"search","type":"int","desc":"搜索","subFieldList":[]}]'
            ],
            'read' => [
                'name' => '详情',
                'method' => 'GET',
                'send' => '[{"name":"id","type":"int","desc":"","subFieldList":[],"must":true}]'
            ],
            'delete' => [
                'name' => '删除',
                'method' => 'GET',
                'send' => '[{"name":"id","type":"int","desc":"","subFieldList":[],"must":true}]'
            ],
            'edit' => [
                'name' => '编辑',
                'method' => 'POST'
            ],
        ];

        $sql = "INSERT INTO `api` ( `projectId`, `parentId`, `type`, `name`, `apiUrl`, `apiType`, `sendData`, `order`, `creator_id`, `modifier_id`, `time`) VALUES";

        $url = '/'.Str::snake($table).'/';
        foreach ($op as $k => $v) {
            $arr = [
                'projectId' => $this->projectId ,
                'parentId' => $pid ,
                'type' => 1 ,
                'name' => $v['name'],
                'apiUrl' => $url . $k ,
                'apiType' => $v['method'] ,
                'sendData' => '[]' ,
                'order' => 1 ,
                'creator_id' => 5 ,
                'modifier_id' => 5 ,
                'time' => time() ,
            ];
            if (!empty($v['send'])){
                $arr['sendData'] = $v['send'];
            } else {
                $send = $this->seedTable($table);
                if ($k == 'create'){
                    foreach ($send as $k => $item) {
                        if ($item['name'] == 'id')
                            unset($send[$k] );
                    }

                }
                foreach ($send as $k => $item) {
                    if ($item['name'] == 'store_id')
                        unset($send[$k] );
                }
                $arr['sendData'] = json_encode(array_values($send) ,JSON_UNESCAPED_UNICODE);
            }
            // halt($arr);
            $sql_arr[] = join("','" , $arr);
        }
        $sql .= "('" . join("'),('" , $sql_arr) . "')";
        $this->execSql("DELETE FROM api WHERE parentId = {$pid}");
        $this->execSql($sql);
    }

    public function seedTable($table)
    {
        $table = \think\helper\Str::snake($table);
        $sql = "SHOW FULL  COLUMNS  FROM `{$table}`";

        $res = Db::query($sql);
        foreach ($res as $re) {
            if (in_array($re['Field'] , ['create_time' , 'update_time' , 'delete_time','creator_id' ]))continue;
            if (strpos($re['Type'] , 'int') !== false)
                $re['Type'] = 'int';
            elseif($re['Type']  == 'json')
                $re['Type'] = 'array';
            else
                $re['Type'] = 'string';
            $arr[] = [
                'name' => $re['Field'],
                'type' => $re['Type'],
                'desc' => $re['Comment'],
                'subFieldList' => [],
                'must' => false
            ];
        }
        return $arr;

    }

    private function execSql($sql)
    {
        $this->db(false)->query($sql);
    }



    public function initApi()
    {
        $this->user();
        $this->role();
        $this->other();
        $this->setting();

    }
    public function user()
    {
        $arr = [
            'projectId' => $this->projectId,
            'type' => 0,
            'parentId' => 0,
            'name' => '人员'
        ];
        $parentId = $this->db()->insertGetId($arr);
        $perArr = [
            1 =>  [
                "type" => 1,
                "name" => "列表",
                "apiUrl" => "/admin.admin/getList",
                "apiType" => "GET",
                "sendData" => '[{"name":"curr","type":"int","desc":"","subFieldList":[]},{"name":"row","type":"int","desc":"","subFieldList":[]},{"name":"is_stop","type":"int","desc":"1有效 2禁用","subFieldList":[]},{"name":"search","type":"int","desc":"","subFieldList":[]}]'
            ],
            2 =>  [
                "type" => 1,
                "name" => "设置角色",
                "apiUrl" => "/admin.admin/setRole",
                "apiType" => "GET",
                "sendData" => '[{"name":"user_id","type":"int","desc":"","subFieldList":[],"must":true},{"name":"role_id","type":"int","desc":"","subFieldList":[],"must":true}]'
            ],
            3 =>  [
                "type" => 1,
                "name" => "重置密码",
                "apiUrl" => "/admin.admin/resetPassword",
                "apiType" => "GET",
                "sendData" => '[{"name":"user_id","type":"int","desc":"","subFieldList":[],"must":true}]'
            ],
            4 =>  [
                "type" => 1,
                "name" => "设置禁用状态",
                "apiUrl" => "/admin.admin/setStop",
                "apiType" => "GET",
                "sendData" => '[{"name":"user_id","type":"int","desc":"","subFieldList":[],"must":true},{"name":"is_stop","type":"int","desc":"1启用2信用","subFieldList":[],"must":true}]'
            ],
            5 =>  [
                "type" => 1,
                "name" => "修改密码自己的",
                "apiUrl" => "/admin.admin/changePassword",
                "apiType" => "GET",
                "sendData" => '[{"name":"old_password","type":"int","desc":"md5","subFieldList":[],"must":true},{"name":"new_password","type":"int","desc":"md5","subFieldList":[],"must":true}]'
            ],
            6 =>  [
                "type" => 1,
                "name" => "修改",
                "apiUrl" => "/admin.admin/edit",
                "apiType" => "GET",
                "sendData" => '[{"name":"id","type":"int","desc":"","subFieldList":[],"must":true},{"name":"mobile","type":"int","desc":"","subFieldList":[],"must":true},{"name":"name","type":"int","desc":"","subFieldList":[],"must":true}]'
            ]
        ];

        array_walk($perArr , function (&$val) use ($parentId ){
            $val['parentId'] = $parentId;
            $val['projectId'] = $this->projectId;
        });

        $this->db()->insertAll($perArr);


    }

    public function role()
    {
        $arr = [
            'projectId' => $this->projectId,
            'type' => 0,
            'parentId' => 0,
            'name' => '角色'
        ];
        $parentId = $this->db()->insertGetId($arr);
        $roleArr = [
            1 => [
                "type" => 1,
                "name" => "列表",
                "apiUrl" => "/admin.roles/getList",
                "apiType" => "GET",
                "sendData" => '[{"name":"curr","type":"int","desc":"","subFieldList":[]},{"name":"row","type":"int","desc":"","subFieldList":[]},{"name":"search","type":"int","desc":"搜索","subFieldList":[]}]',
            ],
            2 => [
                "type" => 1,
                "name" => "详情",
                "apiUrl" => "/admin.roles/read",
                "apiType" => "GET",
                "sendData" => '[{"name":"id","type":"int","desc":"","subFieldList":[],"must":true}]',
            ],
            3 => [
                "type" => 1,
                "name" => "删除",
                "apiUrl" => "/admin.roles/delete",
                "apiType" => "GET",
                "sendData" => '[{"name":"id","type":"int","desc":"","subFieldList":[],"must":true}]',
            ],
            4 => [
                "type" => 1,
                "name" => "编辑",
                "apiUrl" => "/admin.roles/edit",
                "apiType" => "POST",
                "sendData" => '[{"name":"id","type":"int","desc":"","subFieldList":[],"must":false},{"name":"name","type":"string","desc":"","subFieldList":[],"must":true},{"name":"view_sort","type":"int","desc":"排序","subFieldList":[],"must":false},{"name":"is_all","type":"int","desc":"查看全部1是2否","subFieldList":[],"default":"2"}]',
            ],
            5 => [
                "type" => 1,
                "name" => "角色添加权限",
                "apiUrl" => "/admin.roles/addPermission",
                "apiType" => "GET",
                "sendData" => '[{"name":"role","type":"int","desc":"角色id","subFieldList":[],"must":true},{"name":"permission","type":"int","desc":"权限：/login/login","subFieldList":[],"must":true}]',
            ],
            6 => [
                "type" => 1,
                "name" => "角色删除权限",
                "apiUrl" => "/admin.roles/deletePermission",
                "apiType" => "GET",
                "sendData" => '[{"name":"role","type":"int","desc":"","subFieldList":[],"must":true},{"name":"permission","type":"int","desc":"","subFieldList":[],"must":true}]',
            ],
            7 => [
                "type" => 1,
                "name" => "取角色下所有权限",
                "apiUrl" => "/admin.roles/getPermissions",
                "apiType" => "GET",
                "sendData" => '[{"name":"role","type":"int","desc":"","subFieldList":[],"must":true}]',
            ],
            8 => [
                "type" => 1,
                "name" => "用户权限",
                "apiUrl" => "/user/getPermissions",
                "apiType" => "GET",
                "sendData" => '[{"name":"user","type":"int","desc":"1","subFieldList":[]}]',
            ],
            9 => [
                "type" => 1,
                "name" => "取所有权限",
                "apiUrl" => "/admin.roles/getAllPermissions",
                "apiType" => "GET",
                "sendData" => '[]',
            ]
        ];

        array_walk($roleArr , function (&$val) use ($parentId ){
            $val['parentId'] = $parentId;
            $val['projectId'] = $this->projectId;
        });
        $this->db()->insertAll($roleArr);

    }

    public function other()
    {
        $arr = [
            [
                "name"     => "后台登录",
                "apiUrl"   => "/login/login",
                "sendData" => '[{"name":"username","type":"int","desc":"","subFieldList":[],"must":true},{"name":"password","type":"int","desc":"md5","subFieldList":[],"must":true},{"name":"yzm","type":"int","desc":"","subFieldList":[],"must":true}]',
                "desc" => ""
            ],
            [
                "name"   => "登录状态",
                "apiUrl" => "/login/state",
                "sendData" => '',
                "desc" => ""
            ],
            [
                "name"   => "验证码",
                "apiUrl" => "/captcha",
                "sendData" => '',
                "desc" => "get"
            ],
            [
                "name"   => "登出",
                "apiUrl" => "/login/logout",
                "sendData" => '',
                "desc" => ""
            ]
        ];
        array_walk($arr , function (&$val){
            $val['parentId'] = 0;
            $val['type'] = 1;
            $val['projectId'] = $this->projectId;
        });

        $this->db()->insertAll($arr);

    }

    public function t()
    {
        $r = $this->db()->where('id' , 'in' , '2053,2172,2136,2137')->select()->toArray();
        dd($r);

    }

    private function db($table = 'api')
    {
        if ($table)return Db::connect('myApi')->table($table);
        return Db::connect('myApi');
    }

    private function setting()
    {
        $arr = [
            'projectId' => $this->projectId,
            'type' => 0,
            'parentId' => 0,
            'name' => '字典'
        ];
        $parentId = $this->db()->insertGetId($arr);

        $settingArr = [
            0 => [
                "type"     => 1,
                "name"     => "列表",
                "apiUrl"   => "/setting/getList",
                "apiType"  => "GET",
                "sendData" => '[{"name":"curr","type":"int","desc":"","subFieldList":[]},{"name":"row","type":"int","desc":"","subFieldList":[]},{"name":"search","type":"int","desc":"搜索","subFieldList":[]}]',
                "order"    => 1,
            ],
            1 => [
                "type"     => 1,
                "name"     => "详情",
                "apiUrl"   => "/setting/read",
                "apiType"  => "GET",
                "sendData" => '[{"name":"id","type":"int","desc":"","subFieldList":[],"must":true}]',
                "order"    => 1,
            ],
            2 => [
                "type"     => 1,
                "name"     => "删除",
                "apiUrl"   => "/setting/delete",
                "apiType"  => "GET",
                "sendData" => '[{"name":"id","type":"int","desc":"","subFieldList":[],"must":true}]',
                "order"    => 1,
            ],
            3 => [
                "type"     => 1,
                "name"     => "编辑",
                "apiUrl"   => "/setting/edit",
                "apiType"  => "POST",
                "sendData" => '[{"name":"id","type":"int","desc":"","subFieldList":[],"must":false},{"name":"name","type":"string","desc":"角色名称","subFieldList":[],"must":false,"default":""},{"name":"type","type":"string","desc":"访问路径","subFieldList":[],"must":false,"default":""},{"name":"parent_id","type":"int","desc":"父级id","subFieldList":[],"must":false},{"name":"view_sort","type":"int","desc":"排序","subFieldList":[],"must":false}]',
                "order"    => 1,
            ],
            4 => [
                "type"     => 1,
                "name"     => "取下拉",
                "apiUrl"   => "/setting/getSelect",
                "apiType"  => "GET",
                "sendData" => '[{"name":"type","type":"string","desc":"","subFieldList":[],"must":true}]',
                "order"    => 2,
            ],
            5 => [
                "type"     => 1,
                "name"     => "取二级列表",
                "apiUrl"   => "/setting/getAllList",
                "apiType"  => "GET",
                "sendData" => "[]",
                "order"    => 3,
            ]
        ];
        array_walk($settingArr , function (&$val) use ($parentId ){
            $val['parentId'] = $parentId;
            $val['projectId'] = $this->projectId;
        });
        $this->db()->insertAll($settingArr);


    }


}