<?php


namespace yy\auth;


/**
 * 角色
 * Class Roles
 */
class Roles
{


    /*
     * 删除一个角色
     */
    public function deleteRecord($role)
    {
        Permissions::init(0,$role)->deleteRole();
    }

    /*
     * 添加角色权限
     */
    public function addPermission($role , $permission)
    {
        return Permissions::init(0 , $role , $permission )
            ->addPermissionForRole();

    }


    /*
     * 删除权限
     */
    public function deletePermission($role , $permission)
    {
        return Permissions::init(0 , $role , $permission)
            ->deletePermissionsForRole();

    }

    /*
     * 取角色所有权限
     */
    public function getPermissions($role)
    {
        $return = Permissions::init(0 , $role  )
            ->getPermissionsForRole();
        if (empty($return))return[];
        $results = [];
        foreach ($return as $rt) {
            $results[] = '/'. $rt[1] . '/' . $rt[2];
        }
        return $results;


    }

    /*
     * 添加人员角色
     */
    public function addRoleForUser($user,$role)
    {
        return Permissions::init($user, $role)->addRoleForUser();

    }

    /*
     * 删除人员所有角色
     */
    public function deleteAllRolesForUser($user)
    {
        return Permissions::init($user)->deleteAllRolesForUser();

    }

    /*
     * 取人员所有角色
     */
    public function getRolesForUser($user)
    {
        return Permissions::init($user)->getRolesForUser();

    }



}