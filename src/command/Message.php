<?php
declare (strict_types = 1);

namespace yy\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class Message extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('message')
            ->setDescription('定时任务');
    }

    protected function execute(Input $input, Output $output)
    {
        // 推送消息
        \app\model\Message::all();
        // 自动分派
        Order::AutoDistribute();
        trace('定时任务' , 'message');
    }
}
