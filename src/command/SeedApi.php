<?php
declare (strict_types = 1);

namespace yy\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class SeedApi extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('api')
            ->addArgument('table', Argument::OPTIONAL, "your table")
            ->addArgument('name', Argument::OPTIONAL, "name")
            ->setDescription('自动生成api(php think api table name)');
    }

    protected function execute(Input $input, Output $output)
    {
        $projectId = env('other.projectid');
        if (empty($projectId)){
            $output->writeln("请在.env文件中配置other下的projectid");
            exit();
        }
        $table = $input->getArgument('table');
        if ($table == 'init'){
            \yy\build\SeedApi::init($projectId)->initApi();
            exit();
        }
        $name = $input->getArgument('name');
        if (empty($table) || empty($name)){
            $output->writeln("请输入参数。eg:php think api table name");
            exit;
        }
        $table = trim($table);

        $name = trim($name);

        \yy\build\SeedApi::init($projectId)->seed($name, $table);
    }
}
