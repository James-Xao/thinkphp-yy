<?php
declare (strict_types = 1);

namespace yy\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class Build extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('build')
            ->addArgument('table', Argument::OPTIONAL, "your table")
            ->addArgument('name', Argument::OPTIONAL, "your name")
            ->setDescription('自动生成类库(php think build 表名 注释)');
    }

    protected function execute(Input $input, Output $output)
    {
        $table = $input->getArgument('table');
        $name = $input->getArgument('name');
        if (empty($table) || empty($name)){
            $output->writeln("请输入参数。eg:php think build 表名 注释");
            exit;
        }
        $table = trim($table);
        $name = trim($name);
        \yy\build\Build::run($table ,$name);
    }
}
